const complaintsData = [
    {"meterNumber":"1123245",
    "customerName":"John",
    "complaintDate":"03/30/2018",
    "resolutionDate":"03/31/2018",
    "complaint":"Meter Damage",
    "status":"Resolved",
    "comments":"Manufacture defect,replaced"},
    {"meterNumber":"1123245",
    "customerName":"John",
    "complaintDate":"06/02/2018",
    "resolutionDate":"06/02/2018",
    "complaint":"Water Leakage",
    "status":"Resolved",
    "comments":"Pipe damaged,fixed"}
];
export default complaintsData;