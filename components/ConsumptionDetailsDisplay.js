import React from 'react';
import PureChart from 'react-native-pure-chart';
import { StyleSheet, Text, View, Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import { VictoryBar, VictoryChart, VictoryTheme } from "victory-native";
import ChartView from 'react-native-highcharts';
import { Icon } from 'react-native-elements';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
var num = Platform.OS === 'ios' ? 20 : 0
var meters = require('../data/meterData.js').default
let sampleData = [
    { x: 'Jan', y: 30 },
    { x: 'Feb', y: 200 },
    { x: 'Mar', y: 170 },
    { x: 'Apr', y: 250 },
    { x: 'May', y: 160 },
    { x: 'June', y: 310 },
    { x: 'July', y: 210 },
    { x: 'Aug', y: 100 },
    { x: 'Sept', y: 140 },
    { x: 'Oct', y: 510 },
    { x: 'Nov', y: 120 },
    { x: 'Dec', y: 110 }
];
let sampleData1 = [
    ['Jan', 30],
    ['Feb', 200],
    ['Mar', 170],
    ['Apr', 250],
    ['May', 160],
    ['June', 310],
    ['July', 210],
    ['Aug', 100],
    ['Sept', 140],
    ['Oct', 510],
    ['Nov', 120],
    ['Dec', 110]
];
class ConsumptionDetailsDisplay extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('user', '');
        var Highcharts = 'Highcharts';
        var conf = {
            chart: {
                type: 'spline',
                //animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                /*events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }*/
            },
            title: {
                text: 'Consumption Details'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                   month: '%b \'%y'
                   //day: '%e of %b'
                }
                //type: 'datetime',
                //tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                /*plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]*/
            },
            /*tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },*/
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Random data',
                data: [30, 200, 170, 250, 160, 310, 210, 100, 140, 510, 120, 110],
                pointStart: Date.UTC(2017, 0, 1),
                pointInterval: 24 * 3600 * 1000 * 30// one day
            }]
        };

        /*const options = {
            global: {
                useUTC: false
            },
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        };*/
        return (
            <View>

                <View >
                    <Text></Text>
                    <Text></Text>

                    <ChartView style={{ height: 300 }} config={conf} /*options={options}*/></ChartView>
                </View>
            </View>

        );
    }
}
const triggerStyles = {
    triggerText: {
        color: 'white',
        fontSize: 20
    },
    triggerOuterWrapper: {
        //backgroundColor: 'white',
        padding: 5,
        flex: 1,
    },
    triggerWrapper: {
        //backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,

    },
    triggerTouchable: {
        //underlayColor: 'darkblue',
        activeOpacity: 70,
        style: {
            flex: 1,
        },
    },
};
export default withNavigation(ConsumptionDetailsDisplay);