import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableHighlight, Platform, Picker, StatusBar } from 'react-native';
import MapView from 'react-native-maps';
import Search from 'react-native-search-box';
import { withNavigation } from 'react-navigation';
import { List, ListItem, SearchBar, Header, Icon, Button } from "react-native-elements";
import CompanyLogo from '../components/CompanyLogo';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
import _ from 'lodash';
var meters = require('../data/meterData.js').default
var num = Platform.OS === 'ios' ? 20 : 0
var iconSize = Platform.OS === 'ios' ? 35 : 55
const name = "";
class HomePageDisplay extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      filteredList: meters,
      fullList: [],
      query: "",
    }
  }

  renderHeader = () => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-around', backgroundColor: '#003e61' }}>
        <SearchBar
          Platform='default'
          //lightTheme
          round
          inputStyle={{ backgroundColor: 'white', }}
          containerStyle={{ backgroundColor: '#003e61', borderWidth: 0, borderRadius: 0, width: '85%' }}
          searchIcon={{ size: 20 }}
          placeholder='Search'
          onChangeText={this.searchText} />
        <TouchableHighlight onPress={() => {
          this.props.navigation.navigate('AllMeters', { user: name });
        }}>
          <Image style={{ width: 50, height: 50, backgroundColor: '#003e61',paddingTop:20 }}
            source={require('../Assets/geolocalization.png')} />
        </TouchableHighlight>
      </View>
    );

  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
          marginLeft: "1%",
        }}
      />
    );
  };

  searchText = (e) => {
    const query = e.toLowerCase();
    console.log("search string:" + query)
    let fullList = meters;

    let data = fullList.filter((item) => {
      let address = item.address.toLowerCase();
      if (item.meterNumber.match(query) || address.match(query))
        return item;
    })
    console.log(data);
    if (!query || query === '') {
      this.setState({ filteredList: fullList })
    } else if (Array.isArray(data)) {
      this.setState({ filteredList: data })
    }

  };

  render() {

    const { navigation } = this.props;
    name = navigation.getParam('userId', '');

    return (
      <View style={{ flex: 1, backgroundColor: '#0073b0', paddingTop: num }}>
        <StatusBar backgroundColor="#0073b0" />
        <View style={styles.header}>
        <Text> </Text>
        <Text> </Text>
          <Image style={{ width: 50, height: 25, paddingLeft:0 }}
            source={require('../Assets/companyLogo.png')} />
          <Text style={{ fontSize: 25, color: 'white',paddingLeft:40 }}>Meters at Glance</Text>


          <MenuProvider style={{ justifyContent: 'flex-end', alignItems: 'center', paddingLeft:20 }}>
            <View style={{}} >
              <Menu>
                <MenuTrigger text={name} customStyles={triggerStyles} />
                <MenuOptions customStyles={{ marginTop: 50, elevation: 50 ,paddingTop:40}}>
                  <MenuOption onSelect={() => { this.props.navigation.navigate('Login', { username: '', password: '' }) }} text="Logout" />
                  <MenuOption onSelect={() => alert(`Delete`)}>
                    <Text style={{ color: 'red' }}>Delete</Text>
                  </MenuOption>
                  <MenuOption
                    onSelect={() => alert(`Not called`)}
                    disabled={true}
                    text="Disabled"
                  />
                </MenuOptions>
              </Menu>
            </View>
         </MenuProvider>
        </View>



        <View style={{ backgroundColor: 'white', zIndex: 0 }}>
          <FlatList
            data={this.state.filteredList}
            renderItem={({ item }) =>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ paddingLeft: 10 }}>
                  <Icon
                    name='speedometer'
                    type='simple-line-icon'
                    color={((item.unreadDays) >= 100) ? 'red' : (((item.unreadDays) >= 10) ? 'orange' : 'green')}
                    size={iconSize}
                  />
                </View>

                <View style={{ paddingTop: 10, paddingBottom: 10, alignSelf:'flex-start' }}>
                  <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Meter {item.meterNumber}</Text>
                  <Text>{item.address}</Text>
                  <Text style={{ color: ((item.unreadDays) >= 100) ? 'red' : (((item.unreadDays) >= 10) ? 'orange' : 'green') }}>{item.issueWithTheMeter}</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'space-between', paddingRight: 10 }}>
                  <TouchableHighlight onPress={() => {
                    this.props.navigation.navigate('Map', {
                      lat: item.lat,
                      long: item.lon,
                      meterNo: item.meterNumber,
                      desc: item.address,
                      days: item.unreadDays,
                      user: name
                    });
                  }}>
                    <Image style={{ width: 25, height: 25 }}
                      source={require('../Assets/mapIcon.jpeg')} />
                  </TouchableHighlight>
                  <Text> </Text>
                  <TouchableHighlight onPress={() => {
                    this.props.navigation.navigate('MeterDetails', {
                      meterNo: item.meterNumber,
                      installDate: item.installedDate,
                      manufacturer: item.manufacturer,
                      lmrDate: item.lastMeterReadDate,
                      lpDate: item.lastPaymentDate,
                      avgConsumption: item.averageConsumption,
                      avgBillPayment: item.averageBillPayment,
                      user: name,
                      address:item.address,
                      days:item.unreadDays
                    });
                  }
                  }>
                    <Image style={{ width: 25, height: 25 }}
                      source={require('../Assets/detailsIcon.png')} />
                  </TouchableHighlight>

                </View>
              </View>

            }
            ItemSeparatorComponent={this.renderSeparator}
            ListHeaderComponent={this.renderHeader}
          />

        </View>

      </View>


    );
  }

}


const triggerStyles = {
  triggerText: {
    color: 'white',
    fontSize: 20
  },
  triggerOuterWrapper: {
    //backgroundColor: 'white',
    padding: 5,
    flex: 1,
  },
  triggerWrapper: {
    //backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,

  },
  triggerTouchable: {
    //underlayColor: 'darkblue',
    activeOpacity: 70,
    style: {
      flex: 1,
    },
  },
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    //backgroundColor: '#ffb6c1',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  header: {
    //flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    //alignItems: 'center',
    backgroundColor: '#0073b0',
    zIndex: 1,

  },
  body: {
    //flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  header2: {
    //flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    //alignItems: 'center',
    backgroundColor: 'skyblue',
  }
});

export default withNavigation(HomePageDisplay);



