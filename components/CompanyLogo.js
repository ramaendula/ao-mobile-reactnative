        import React from 'react';
        import {Image} from 'react-native';


        class CompanyLogo extends React.Component {
            render() {
            return (
                <Image
                source = {require('../Assets/ao-brand-logo-horizontal256x256.png')} 
                //source={require('./Assets/ao-brand-mark4096x4096.png')}
                style={{ width: 100, height: 20 }}
                />
            );
            }
        }
        export default CompanyLogo;