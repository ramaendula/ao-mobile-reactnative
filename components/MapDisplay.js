import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, Platform } from 'react-native';
import { Icon } from 'react-native-elements';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { withNavigation } from 'react-navigation';
import Button from 'react-native-button';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
var num = Platform.OS === 'ios' ? 20 : 0

class MapDisplay extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const { navigation } = this.props;
        const lat = navigation.getParam('lat', '37.78825');
        const long = navigation.getParam('long', '-122.4324');
        const meterNo = navigation.getParam('meterNo', '');
        const desc = navigation.getParam('desc', '');
        const unreadDays = navigation.getParam('days', '8888');
        const name = navigation.getParam('user', '');
        console.log("Number of unread days :" + unreadDays);
        console.log("Address :" + desc);
        return (
            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-start', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#0073b0', zIndex: 1, paddingTop: num }}>
                <Icon
                    name='arrow-left'
                    type='simple-line-icon'
                    color={'white'}
                    size={20}
                    onPress={() => {
                        this.props.navigation.navigate('Home')}}
                  />
                    <Text></Text>
                    <Text style={{ fontSize: 25, color: 'white',paddingLeft:70}}>Meters at Glance</Text>


                    <MenuProvider style={{ justifyContent: 'flex-end', alignItems: 'center',paddingLeft:30 }}>
                        <View style={{}} >
                            <Menu>
                                <MenuTrigger text={name} customStyles={triggerStyles} />
                                <MenuOptions customStyles={{ marginTop: 50, elevation: 50 }}>
                                    <MenuOption onSelect={() => { this.props.navigation.navigate('Login', { username: '', password: '' }) }} text="Logout" />
                                    <MenuOption onSelect={() => alert(`Delete`)}>
                                        <Text style={{ color: 'red' }}>Delete</Text>
                                    </MenuOption>
                                    <MenuOption
                                        onSelect={() => alert(`Not called`)}
                                        disabled={true}
                                        text="Disabled"
                                    />
                                </MenuOptions>
                            </Menu>
                        </View>


                    </MenuProvider>
                </View>
                <MapView
                    style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute' }}
                    region={{
                        latitude: parseFloat(lat),
                        longitude: parseFloat(long),
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}>
                    <MapView.Marker
                        //image = {require('../Assets/meterIcon.png') }
                        //<Image source={require('src/assets/marker.png')} style={{ width: 40, height: 40 }} />
                        coordinate={{ latitude: parseFloat(lat), longitude: parseFloat(long) }}
                        title={meterNo}
                        description={desc}
                        pinColor={((unreadDays) >= 100) ? 'red' : (((unreadDays) >= 10) ? 'orange' : 'green')}
                        //image = {require('../Assets/meterIcon.png')} style={{ width: 40, height: 40, tintColor: ((unreadDays) >= 100) ? 'red' : (((unreadDays) >= 10) ? 'orange' : 'green') }}
                    >
                        <Image source={require('../Assets/meterIcon.png')} style={{ width: 40, height: 40, tintColor: ((unreadDays) >= 100) ? 'red' : (((unreadDays) >= 10) ? 'orange' : 'green') }} />
                        <MapView.Callout style={{height:130 }}>
                            <View style={{  }}>
                                <Text style={{ fontSize: 25 ,fontWeight: 'bold' }}>{meterNo}</Text>
                                <Text>{desc}</Text>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' ,paddingTop:10}}>
                                    <TouchableHighlight onPress={() => { this.props.navigation.navigate('ConsumptionDetails') }}>
                                        <Image style={{ width: 50, height: 50 ,tintColor:'#062c51'}}
                                            source={require('../Assets/consumptionIcon.png')} />
                                    </TouchableHighlight>



                                    <TouchableHighlight onPress={() => { this.props.navigation.navigate('PaymentDetails') }}>
                                        <Image style={{ width: 50, height: 50,tintColor:'#062c51' }}
                                            source={require('../Assets/paymentIcon.png')} />
                                    </TouchableHighlight>



                                    <TouchableHighlight onPress={() => { this.props.navigation.navigate('Complaints') }}>
                                        <Image style={{ width: 50, height: 50 ,tintColor:'#062c51'}}
                                            source={require('../Assets/complaintsIcon.png')} />
                                    </TouchableHighlight>
                                </View>
                            </View>
                        </MapView.Callout>
                    </MapView.Marker>
                </MapView>
            </View >
        );
    }

}
const triggerStyles = {
    triggerText: {
        color: 'white',
        fontSize: 20
    },
    triggerOuterWrapper: {
        //backgroundColor: 'white',
        padding: 5,
        flex: 1,
    },
    triggerWrapper: {
        //backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,

    },
    triggerTouchable: {
        //underlayColor: 'darkblue',
        activeOpacity: 70,
        style: {
            flex: 1,
        },
    },
};

export default withNavigation(MapDisplay);