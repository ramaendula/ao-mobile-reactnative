import React from 'react';
import PureChart from 'react-native-pure-chart';
import { StyleSheet, Text, View, Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import { VictoryBar, VictoryChart, VictoryTheme } from "victory-native";
import { Icon } from 'react-native-elements';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
var num = Platform.OS === 'ios' ? 20 : 0
var meters = require('../data/meterData.js').default

let sampleData = [
    {x: 'Jan', y: 30},
    {x: 'Feb', y: 200},
    {x: 'Mar', y: 170},
    {x: 'Apr', y: 250},
    {x: 'May', y: 10},
    {x: 'June', y: 10},
    {x: 'July', y: 10},
    {x: 'Aug', y: 10},
    {x: 'Sept', y: 10},
    {x: 'Oct', y: 10},
    {x: 'Nov', y: 10},
    {x: 'Dec', y: 10}
];
class PaymentDetailsDisplay extends React.Component {
    constructor(props) {
        super(props)
    }
    
render() {
    const { navigation } = this.props;
    const name = navigation.getParam('user', '');
      return (
          <View style={{flexDirection:'column',alignItems:'center',justifyContent: 'center'}}>
          <Text></Text>
          <Text></Text>
        <PureChart  
           width={'90%'}
           height={100}  
           data={sampleData} 
           type='line' /> 
           <VictoryChart width={400} theme={VictoryTheme.material}>
          <VictoryBar data={meters[0].consumptionDetails} x="x" y="y"  style={{ data: { fill: "#c43a31" , stroke: "black", strokeWidth: 2} }}/>
        </VictoryChart> 
        </View>
      );
  }
}
const triggerStyles = {
    triggerText: {
        color: 'white',
        fontSize: 20
    },
    triggerOuterWrapper: {
        //backgroundColor: 'white',
        padding: 5,
        flex: 1,
    },
    triggerWrapper: {
        //backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,

    },
    triggerTouchable: {
        //underlayColor: 'darkblue',
        activeOpacity: 70,
        style: {
            flex: 1,
        },
    },
};
export default withNavigation(PaymentDetailsDisplay);