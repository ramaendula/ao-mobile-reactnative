import React from 'react';
import PureChart from 'react-native-pure-chart';
import { StyleSheet, Text, View , ListView,ScrollView,Platform} from 'react-native';
import { withNavigation } from 'react-navigation';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Icon } from 'react-native-elements';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
var num = Platform.OS === 'ios' ? 20 : 0
var meters = require('../data/complaintsData.js').default

let sampleData = [
    {x: 'Jan', y: 30},
    {x: 'Feb', y: 200},
    {x: 'Mar', y: 170},
    {x: 'Apr', y: 250},
    {x: 'May', y: 10},
    {x: 'June', y: 10},
    {x: 'July', y: 10},
    {x: 'Aug', y: 10},
    {x: 'Sept', y: 10},
    {x: 'Oct', y: 10},
    {x: 'Nov', y: 10},
    {x: 'Dec', y: 10}
];
class ComplaintsDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      widthArr: [60, 80, 80, 100, 120, 140, 160],
      tableHead: ['Meter Number', 'Customer Name', 'Complaint Date', 'Resolution Date', 'Complaint', 'Status', 'Comments'],
      tableData: [
        ['1', 'john', '2/12/2018', '2/13/2018', 'meter damage', 'resolved', 'manufacture defect,replaced'],
        ['2', 'john', '2/12/2018', '2/13/2018', 'meter damage', 'resolved', 'manufacture defect,replaced'],
        ['3', 'john', '2/12/2018', '2/13/2018', 'meter damage', 'resolved', 'manufacture defect,replaced'],
        ['4', 'john', '2/12/2018', '2/13/2018', 'meter damage', 'resolved', 'manufacture defect,replaced']
      ]
    }
  }
  render() {
    const { navigation } = this.props;
    const name = navigation.getParam('user', '');
    return (
      <View style = {{flex:1}}>
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{ borderColor: '#C1C0B9' }}>
              <Row data={this.state.tableHead} widthArr={this.state.widthArr} style={styles.header} textStyle={styles.text} />
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                {
                  this.state.tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data={rowData}
                      widthArr={this.state.widthArr}
                      style={[styles.row, index % 2 && { backgroundColor: '#F7F6E7' }]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: '#537791' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' }
});
const triggerStyles = {
  triggerText: {
      color: 'white',
      fontSize: 20
  },
  triggerOuterWrapper: {
      //backgroundColor: 'white',
      padding: 5,
      flex: 1,
  },
  triggerWrapper: {
      //backgroundColor: 'blue',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,

  },
  triggerTouchable: {
      //underlayColor: 'darkblue',
      activeOpacity: 70,
      style: {
          flex: 1,
      },
  },
};
export default withNavigation(ComplaintsDisplay);