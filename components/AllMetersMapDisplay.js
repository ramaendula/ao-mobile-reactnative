import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Image, Platform, Geolocation } from 'react-native';
import { Icon } from 'react-native-elements';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { withNavigation } from 'react-navigation';
//import {meterData} from '../data/meterData.js';
import Button from 'react-native-button';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
import PropTypes from "prop-types";
import geolib from "geolib";

var num = Platform.OS === 'ios' ? 20 : 0
var zoom = 0, centerX = 0, centerY = 0, n = 0, tileX = 0, circle, tileY = 0, locX = 0, locY = 0, pixX = 0, pixY = 0, dist = 0, k = 0, X = 0, Y = 0, length = 9, token = 0, j = 0, arr = [], arr1 = [], ticket = 0, arr2 = [];

var meters = require('../data/meterData.js').default
var markerdump = [],markers = [],markersClone =[];
{meters.map((meter,index) => 
    markerdump[index] = {latitude:parseFloat(meter.lat),longitude:parseFloat(meter.lon),unreadDays:parseInt(meter.unreadDays)})
 }
 
 /*for(i=0;i<=meters.length;i++){
     markerdump[i] = {latitude:parseFloat(meters[i].lat),longitude:parseFloat(meters[i].lon)}
 }*/
class AllMetersMapDisplay extends React.Component {
    constructor(props) {
        super(props)

        this.innerHandleClick = this.innerHandleClick.bind(this);
        this.outerHandleClick = this.outerHandleClick.bind(this);

        this.state = {
            r: 1000,
            center: { latitude: 0, longitude: 0 },
            markers:markerdump,
            markersClone:markerdump,
            
        }

    }

    innerHandleClick() {
        token = 1;
    }

    outerHandleClick() {
        token = 2;
    }

    onMapPress(e) {
        this.setState({markers:this.state.markersClone})
    }
    onMapLongPress(e) {
        if(token!==0)
        {
            console.log(this.state.markers)
            arr=this.state.markersClone;
            this.setState({center:{latitude:e.nativeEvent.coordinate.latitude,longitude:e.nativeEvent.coordinate.longitude}});
            for(k=0;k<arr.length;k++)
            {
              X=arr[k].latitude;
              Y=arr[k].longitude;
              circle=geolib.isPointInCircle({latitude:X,longitude:Y},{latitude:e.nativeEvent.coordinate.latitude,longitude:e.nativeEvent.coordinate.longitude},this.state.r);
              if(circle===true){
                arr1.push(arr[k]);
              }else if(circle===false){
                arr2.push(arr[k]);
              }
            }
            if(token===1){
               this.setState({markers:arr1});
            }else if(token===2){
               this.setState({markers:arr2});
            }
        
            arr1=[];
            arr2=[];
        }
        
    }
    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('user', '');
        return (
            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-start', alignItems: 'center', }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#0073b0', zIndex: 1, paddingTop: num }}>
                    <Icon
                        name='arrow-left'
                        type='simple-line-icon'
                        color={'white'}
                        size={20}
                        onPress={() => {
                            this.props.navigation.navigate('Home')
                        }}
                    />
                    <Text></Text>
                    <Text style={{ fontSize: 25, color: 'white', paddingLeft: 70 }}>Meters at Glance</Text>


                    <MenuProvider style={{ justifyContent: 'flex-end', alignItems: 'center', paddingLeft: 30 }}>
                        <View style={{}} >
                            <Menu>
                                <MenuTrigger text={name} customStyles={triggerStyles} />
                                <MenuOptions customStyles={{ marginTop: 50, elevation: 50 }}>
                                    <MenuOption onSelect={() => { this.props.navigation.navigate('Login', { username: '', password: '' }) }} text="Logout" />
                                    <MenuOption onSelect={() => alert(`Delete`)}>
                                        <Text style={{ color: 'red' }}>Delete</Text>
                                    </MenuOption>
                                    <MenuOption
                                        onSelect={() => alert(`Not called`)}
                                        disabled={true}
                                        text="Disabled"
                                    />
                                </MenuOptions>
                            </Menu>
                        </View>


                    </MenuProvider>
                </View>
                <MapView
                    style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute' }}
                    initialRegion={{
                        latitude: 40.3853963,
                        longitude: -79.96264,
                        latitudeDelta: 0.0491,
                        longitudeDelta: 0.375,
                    }}
                    onPress = {this.onMapPress.bind(this)}
                    onLongPress={this.onMapLongPress.bind(this)}
                >

                    {this.state.markers.map((meter, index) =>
                        <MapView.Marker
                        coordinate={{
                            latitude: meter.latitude,
                            longitude: meter.longitude,
                        }}
                            title={meter.meterNumber}
                            description={meter.address}
                            pinColor={((meter.unreadDays) >= 100) ? 'red' : (((meter.unreadDays) >= 10) ? 'orange' : 'green')}
                            key={index}
                        >
                            <Image source={require('../Assets/meterIcon.png')} style={{ width: 40, height: 40, tintColor: ((meter.unreadDays) >= 100) ? 'red' : (((meter.unreadDays) >= 10) ? 'orange' : 'green') }} />
                            <MapView.Callout style={{ height: 130, width: 150 }}>
                                <View>
                                    <Text style={{ fontWeight: 'bold' }}>{meter.meterNumber}</Text>
                                    <Text>{meter.address}</Text>
                                    <Text></Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                        <TouchableHighlight onPress={() => { this.props.navigation.navigate('ConsumptionDetails') }}>
                                            <Image style={{ width: 50, height: 40, tintColor: '#062c51' }}
                                                source={require('../Assets/consumptionIcon.png')} />
                                        </TouchableHighlight>


                                        <Button
                                            containerStyle={{ padding: 5, height: 25, overflow: 'hidden', borderRadius: 1, backgroundColor: 'white' }}
                                            disabledContainerStyle={{ backgroundColor: 'grey' }}
                                            style={{ fontSize: 10, color: 'green' }}
                                            onPress={() => alert("clicked")}>

                                        </Button>

                                        <TouchableHighlight onPress={() => { this.props.navigation.navigate('PaymentDetails') }}>
                                            <Image style={{ width: 50, height: 40, tintColor: '#062c51' }}
                                                source={require('../Assets/paymentIcon.png')} />
                                        </TouchableHighlight>


                                        <Button
                                            containerStyle={{ padding: 5, height: 25, overflow: 'hidden', borderRadius: 1, backgroundColor: 'white' }}
                                            disabledContainerStyle={{ backgroundColor: 'grey' }}
                                            style={{ fontSize: 10, color: 'green' }}
                                            onPress={() => alert("clicked")}>

                                        </Button>

                                        <TouchableHighlight onPress={() => { this.props.navigation.navigate('Complaints') }}>
                                            <Image style={{ width: 50, height: 40, tintColor: '#062c51' }}
                                                source={require('../Assets/complaintsIcon.png')} />
                                        </TouchableHighlight>

                                    </View>
                                </View>
                            </MapView.Callout>
                        </MapView.Marker>
                    )}
                    <MapView.Circle 
                        center={this.state.center}
                        radius={RADIUS=this.state.r}
                        strokeWidth={1}
                        strokeColor={ '#1a66ff' }
                        fillColor = { 'rgba(230,238,255,0.5)' } />
                    </MapView>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <Button
                        style={{ fontSize: 10, color: 'green' }}
                        containerStyle={{ padding: 5, height: 25, overflow: 'hidden', borderRadius: 1, backgroundColor: 'skyblue' }}
                        onPress={this.innerHandleClick}>
                        Inner meters
                    </Button>
                    <Button
                        style={{ fontSize: 10, color: 'green' }}
                        containerStyle={{ padding: 5, height: 25, overflow: 'hidden', borderRadius: 1, backgroundColor: 'skyblue' }}
                        onPress={() => alert("clicked")}>
                        Draw a circle
                    </Button>
                    <Button
                        style={{ fontSize: 10, color: 'green' }}
                        containerStyle={{ padding: 5, height: 25, overflow: 'hidden', borderRadius: 1, backgroundColor: 'skyblue' }}
                        onPress={this.outerHandleClick}>
                        outer meters
                    </Button>
                </View>
            </View>
        );
    }
}
const triggerStyles = {
    triggerText: {
        color: 'white',
        fontSize: 20
    },
    triggerOuterWrapper: {
        //backgroundColor: 'white',
        padding: 5,
        flex: 1,
    },
    triggerWrapper: {
        //backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,

    },
    triggerTouchable: {
        //underlayColor: 'darkblue',
        activeOpacity: 70,
        style: {
            flex: 1,
        },
    },
};
export default withNavigation(AllMetersMapDisplay);