import React from 'react';
import { StyleSheet, Text, View, FlatList, Image ,TouchableHighlight ,Platform} from 'react-native';
import { withNavigation } from 'react-navigation';
//import { Button} from 'react-native-elements';
import { Icon } from 'react-native-elements';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';
var num = Platform.OS === 'ios' ? 20 : 0
var iconSize = Platform.OS === 'ios' ? 55 : 55

class MeterDetailsDisplay extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { navigation } = this.props;
        const meterNumber = navigation.getParam('meterNo', '');
        const installedDate = navigation.getParam('installDate', '');
        const manufacturer = navigation.getParam('manufacturer', '');
        const lastMeterReadDate = navigation.getParam('lmrDate', '');
        const lastPaymentDate = navigation.getParam('lpDate', '');
        const averageConsumtion = navigation.getParam('avgConsumption', '');
        const averageBillPayment = navigation.getParam('avgBillPayment', '');
        const name = navigation.getParam('user','');
        const address = navigation.getParam('address','');
        const unreadDays = navigation.getParam('days','');
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#0073b0', zIndex: 1, paddingTop: num }}>
                <Icon
                    name='arrow-left'
                    type='simple-line-icon'
                    color={'white'}
                    size={20}
                    onPress={() => {
                        this.props.navigation.navigate('Home')}}
                  />
                    <Text style={{ fontSize: 25, color: 'white',paddingLeft:70}}>Meters at Glance</Text>


                    <MenuProvider style={{ justifyContent: 'flex-end', alignItems: 'center',paddingLeft:30 }}>
                        <View style={{}} >
                            <Menu>
                                <MenuTrigger text={name} customStyles={triggerStyles} />
                                <MenuOptions customStyles={{ marginTop: 50, elevation: 50 }}>
                                    <MenuOption onSelect={() => { this.props.navigation.navigate('Login', { username: '', password: '' }) }} text="Logout" />
                                    <MenuOption onSelect={() => alert(`Delete`)}>
                                        <Text style={{ color: 'red' }}>Delete</Text>
                                    </MenuOption>
                                    <MenuOption
                                        onSelect={() => alert(`Not called`)}
                                        disabled={true}
                                        text="Disabled"
                                    />
                                </MenuOptions>
                            </Menu>
                        </View>


                    </MenuProvider>
                </View>
                <View style = {{flexDirection:'row',paddingTop:20,backgroundColor:'#f0f4f7'}}>
                    <Icon
                         name='speedometer'
                         type='simple-line-icon'
                          color={((unreadDays) >= 100) ? 'red' : (((unreadDays) >= 10) ? 'orange' : 'green')}
                         size={iconSize}
                    />
                    <View style = {{paddingLeft:30}}>
                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{meterNumber}</Text>
                        <Text style={{ fontSize: 25, fontWeight: 'bold'}}>{address}</Text>
                    </View>
                </View>
                <Text style={{backgroundColor:'#f0f4f7'}}></Text>
                <Text style={{backgroundColor:'#f0f4f7'}}></Text>
                <Text style={{backgroundColor:'#f0f4f7'}}></Text>
                <View style={{ borderStyle: 'solid', borderColor: 'grey', backgroundColor: "#ecf1f4",borderRadius: 2, borderWidth: 2, backfaceVisibility: 'visible',paddingTop:0 }}>
                    <Text style={{ fontSize: 20 ,paddingTop:20 }}>                Installed Date : {installedDate}</Text>
                    <Text style={{ fontSize: 20 }}>                 Manufacturer : {manufacturer}</Text>
                    <Text style={{ fontSize: 20 }}>       Last Payment date : {lastPaymentDate}</Text>
                    <Text style={{ fontSize: 20 }}> Last Meter Read Date : {lastMeterReadDate}</Text>
                    <Text style={{ fontSize: 20 }}>Average Consumption : {averageConsumtion}</Text>
                    <Text style={{ fontSize: 20 ,paddingBottom:20}}>  Average Bill Payment : {averageBillPayment}</Text>
                </View>
                <Text style={{backgroundColor:'#f0f4f7'}}></Text>
                <View style={styles.buttonView}>
                    <TouchableHighlight onPress={() => { this.props.navigation.navigate('ConsumptionDetails'),{user:name} }}>
                        <Image style={{ width: 50, height: 60, tintColor:"#003e61" }}
                            source={require('../Assets/consumptionIcon.png')} />
                    </TouchableHighlight>

                    <TouchableHighlight onPress={() => { this.props.navigation.navigate('PaymentDetails'),{user:name} }}>
                        <Image style={{ width: 50, height: 60, tintColor:"#003e61" }}
                            source={require('../Assets/paymentIcon.png')} />
                    </TouchableHighlight>

                    <TouchableHighlight onPress={() => { this.props.navigation.navigate('Complaints') ,{user:name}}}>
                        <Image style={{ width: 50, height: 60, tintColor:"#003e61" }}
                            source={require('../Assets/complaintsIcon.png')} />
                    </TouchableHighlight>


                    
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f0f4f7',
        //alignItems: 'flex-start',
        //justifyContent: 'flex-start',
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingTop:30,
        backgroundColor:'#f0f4f7'
    }
});
const triggerStyles = {
    triggerText: {
        color: 'white',
        fontSize: 20
    },
    triggerOuterWrapper: {
        //backgroundColor: 'white',
        padding: 5,
        flex: 1,
    },
    triggerWrapper: {
        //backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,

    },
    triggerTouchable: {
        //underlayColor: 'darkblue',
        activeOpacity: 70,
        style: {
            flex: 1,
        },
    },
};

export default withNavigation(MeterDetailsDisplay);