import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button, Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import { KeyboardAvoidingView } from 'react-native';
var SQLite = require('react-native-sqlite-storage')
var db = Platform.OS === 'ios' ? (SQLite.openDatabase({name:"loginCredentials.db",createFromLocation:1})) : (SQLite.openDatabase({name:"loginCredentials.db",createFromLocation:"~loginCredentials.db"}))
var users = require('../data/users.js').default

class LoginPageDisplay extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      userName: "",
      password: "",
      passwordFromDB: "",
      
    }

    //db = SQLite.openDatabase({ name: "loginCredentials", createFromLocation:1});
    console.log("starting transaction.......");
    db.transaction((tx) => {
      console.log("Querying starting.......");
      tx.executeSql('SELECT * FROM Users', [], (tx, results) => {
        console.log("Query completed");
        var len = results.rows.length;
        console.log("length:"+len);
        console.log(results.rows.item(0));
        if (len > 0) {
          var row = results.rows.item(0);
          this.setState({ passwordFromDB: row.password});
          console.log("password from database:"+passwordFromDB);
        }
      });
    });
  }

  



  userNameChangeHandler = val => {
    this.setState({
      userName: val
    });
  }

  passwordChangeHandler = val => {
    this.setState({
      password: val
    });
  }

  render() {
    const { navigation } = this.props;
    const username1 = navigation.getParam('username', '');
    //this.userNameChangeHandler(username1);
    //this.attendee.setNativeProps({ text: '' })
    const password1 = navigation.getParam('password', '');
    return (
      //<View style={styles.container}>
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <Image source={require('../Assets/ao-brand-logo-horizontal256x256.png')} />
        <TextInput
          style={{ height: 40, width: 300, borderColor: 'gray', borderWidth: 1 }}
          autoCapitalize = "none"
          onChangeText={this.userNameChangeHandler}
          value={this.state.userName}
          placeholder="Username"
          clearButtonMode="always"
          ref={element => {
            this.attendee = element
          }}
        />
        <Text /><Text />
        <TextInput
          secureTextEntry={true}
          style={{ height: 40, width: 300, borderColor: 'gray', borderWidth: 1 }}
          autoCapitalize = "none"
          onChangeText={this.passwordChangeHandler}
          value={this.state.password}
          placeholder="Password"
          clearButtonMode="always"
        />
        <Text /><Text />
        <Button
          title="Login"
          onPress={() => {
            console.log(this.state.password)
            console.log(this.state.passwordFromDB)
            if (this.state.userName == '' || this.state.password == '') {
              alert('Enter a valid username and password');
            } //else if (this.state.password == this.state.passwordFromDB) {
              else if (this.state.password == users[0].password && this.state.userName == users[0].username) {
              this.props.navigation.navigate('Home', {
                //userId: this.state.userName
                userId:users[0].nameOfTheUser
              });
            } else {
              alert('You have entered a wrong password');
            }
          }
          }
        />
      </KeyboardAvoidingView>
      //</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  }
});



export default withNavigation(LoginPageDisplay);
