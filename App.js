import React from 'react';
import { StyleSheet, Text, View, FlatList, Button } from 'react-native';
import MapView from 'react-native-maps';
import Search from 'react-native-search-box';
import { createStackNavigator } from 'react-navigation';
import LoginPageDisplay from './components/LoginPageDisplay';
import HomePageDisplay from './components/HomePageDisplay';
import MapDisplay from './components/MapDisplay';
import CompanyLogo from './components/CompanyLogo';
import AllMetersMapDisplay from './components/AllMetersMapDisplay';
import MeterDetailsDisplay from './components/MeterDetailsDisplay';
import ConsumptionDetailsDisplay from './components/ConsumptionDetailsDisplay';
import PaymentDetailsDisplay from './components/PaymentDetailsDisplay';
import ComplaintsDisplay from './components/ComplaintsDisplay';
//import { Button ,Icon} from 'react-native-elements';

class LoginPage extends React.Component {
  render() {
    return (
      <LoginPageDisplay />
    );
  }
}

class HomePage extends React.Component {
  render() {
    return (
      <HomePageDisplay />
    );
  }
}

class MapPage extends React.Component {
  render() {
    return (
      <MapDisplay />
    );
  }
}

class AllMetersPage extends React.Component {
  render() {
    return (
      <AllMetersMapDisplay />
    );
  }
}

class MeterDetailsPage extends React.Component {
  render() {
    return (
      <MeterDetailsDisplay />
    );
  }
}

class ConsumptionDetailsPage extends React.Component {
  render() {
    return (
      <ConsumptionDetailsDisplay />
    );
  }
}

class PaymentDetailsPage extends React.Component {
  render() {
    return (
      <PaymentDetailsDisplay />
    );
  }
}

class ComplaintsPage extends React.Component {
  render() {
    return (
      <ComplaintsDisplay />
    );
  }
}
const RootStack = createStackNavigator(
  {
    Login: LoginPage,
    Home: {
      screen: HomePage,
      navigationOptions: {
        header: null,
      }
    },
    Map:{
      screen: MapPage,
      navigationOptions:{
        header: null,
      }
   },
    AllMeters:{
      screen:AllMetersPage,
      navigationOptions:{
        header:null,
      }
    } ,
    MeterDetails: {
      screen: MeterDetailsPage,
      navigationOptions: {
        header:null,
      }
    },
    ConsumptionDetails:{
      screen:ConsumptionDetailsPage,
      navigationOptions:{
        //header:null,
        title: 'Consumption Details',
        headerStyle: {
          backgroundColor: '#0073b0',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }
    },
    PaymentDetails:{
      screen:PaymentDetailsPage,
      navigationOptions:{
        //header:null,
        title: 'Payment Details',
        headerStyle: {
          backgroundColor: '#0073b0',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }
    },
    Complaints:{
      screen:ComplaintsPage,
      navigationOptions:{
        //header:null,
        title: 'Complaints',
        headerStyle: {
          backgroundColor: '#0073b0',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }
    }
  },
  {
    initialRouteName: 'Login',
  }
);

export default class App extends React.Component {
  render() {
    return (
      <RootStack />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
